from aioredis import Channel, Redis
from fastapi import FastAPI
from fastapi.params import Depends
from fastapi_plugins import depends_redis, redis_plugin
from sse_starlette.sse import EventSourceResponse
from starlette.responses import HTMLResponse

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>SSE</title>
    </head>
    <body>
        <script>
            const evtSource = new EventSource("http://localhost:64669/sse/stream");
            evtSource.addEventListener("message", function(event) {
                // Logic to handle status updates
                console.log(event.data)
            });  
        </script>
    </body>
</html>
"""
import json
import aioredis
app = FastAPI()


@app.on_event("startup")
async def on_startup() -> None:
    await redis_plugin.init_app(app)
    await redis_plugin.init()


@app.on_event("shutdown")
async def on_shutdown() -> None:
    await redis_plugin.terminate()


@app.get("/")
async def get():
    return HTMLResponse(html)

async def redis_connect():
    return await aioredis.create_redis_pool('redis://localhost:6379')

@app.get("/sse/publish")
async def get(channel: str = "default", redis: Redis = Depends(redis_connect)):
    await redis.publish(channel=channel, message=json.dumps({'a':12,'b':'dasd'}))
    return ""


@app.get("/sse/stream")
async def stream(channel: str = "default", redis: Redis = Depends(redis_connect)):
    return EventSourceResponse(subscribe(channel, redis))


async def subscribe(channel: str, redis: Redis):
    (channel_subscription,) = await redis.subscribe(channel=Channel(channel, False))
    while await channel_subscription.wait_message():
        yield {"event": "message", "data": await channel_subscription.get()}